var now = new Date()
var thisYear = now.getFullYear()
function arrayToObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        rv[(i+1)+". "+arr[i][0]+" "+arr[i][1]] = {
          firstName: arr[i][0],
          lastName: arr[i][1],
          gender: arr[i][2],
          age: (thisYear-arr[i][3]<0||arr[i][3]==null)?("Invalid Birth Year"):(thisYear-arr[i][3])
        };
    console.log(rv)
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
function shoppingTime(memberId, money) {
    if(memberId==null||memberId==""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup"
    }else{
        var changeMoney=money
        var listPurchased = []
        for (var i = 0; i < 5; ++i){
        if(changeMoney>=1500000){
            listPurchased.push("Sepatu Stacattu")
            changeMoney-=1500000
        }else if(changeMoney>=500000){
            listPurchased.push("Baju Zoro")
            changeMoney-=500000
        }else if(changeMoney>=250000){
            listPurchased.push("Baju H&N")
            changeMoney-=250000
        }else if(changeMoney>=175000){
            listPurchased.push("Sweater Uniklooh")
            changeMoney-=175000
        }else if(changeMoney>=50000){
            listPurchased.push("Casing Handphone")
            changeMoney-=50000
        }
    }

        return personObj = {
            memberId : memberId,
            money: money,
            listPurchased: listPurchased,
            changeMoney: changeMoney
        } 
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
function naikAngkot(arr) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var rv = [];
    for (var i = 0; i < arr.length; ++i)
        rv.push({
            penumpang: arr[i][0],
            naikDari: arr[i][1],
            tujuan: arr[i][2],
            bayar: (rute.indexOf(arr[i][2])-rute.indexOf(arr[i][1]))*2000
            });
    return rv
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]