function sum(startNum, finishNum,step) {
    var hobbies = 0;
    if(startNum==null){
        return 0;
    }
    if(finishNum==null){
        return startNum;
    }
    if(step==null){
        step=1;
    }
    if(startNum<finishNum){
        for(var angka = startNum; angka <= finishNum; angka+=step) {
            hobbies+=angka;
        }
    }else{
        for(var angka = startNum; angka >= finishNum; angka-=step) {
            hobbies+=angka;
        }
    }
    return hobbies;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 